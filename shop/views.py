from django.db.models import Max ,Avg,Q

from django.shortcuts import render
from  decimal import Decimal as D
# Create your views here.
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404

from cart.cart import Cart
from .models import Category, Product,Colors


def product_list(request, category_slug=None):
    category = None

    cart = Cart(request)
    categories = Category.objects.all()
    products = Product.objects.filter(available=True)
    if category_slug:
       category = get_object_or_404(Category, slug=category_slug)
       products = products.filter(category=category)
    return render(request,
    'shop/product/list.html',
    {'category': category,'categories': categories,'products': products,'cart':cart})


def product_detail(request, id, slug):
    cart = Cart(request)
    product = get_object_or_404(Product, id=id,slug=slug, available=True)
    category=product.category
    products = Product.objects.filter(category=category)

    return render(request,'shop/product/detail.html',{'product': product,'cart':cart,'category':products})

# def fiter_product(request):
#     cart = Cart(request)
#     start_date = D(request.GET.get('start_date',0))
#     end_date = D(request.GET.get('end_date',0))
#     if start_date and end_date:
#         products = Product.objects.filter(price__range=(start_date,end_date))
#         return render(request,
#                       'shop/product/list.html',
#                       { 'pro': products, 'cart': cart})

# def fiter_product(request):
#     cart = Cart(request)
#     product = Product.objects.all().aggregate(Max('price'))
#     {'price__max':D('30.10')}
#     return render(request,'shop/product/list.html',{'product': product,'cart':cart})



def filter_product_price(request):
    cart = Cart(request)
    categories = Category.objects.all()
    price1 = request.GET.get('min_price', 0)
    price2 = request.GET.get('max_price', 0)

    if not price2 or not price1:
        my_products_price = Product.objects.all()

    else:
      my_products_price = Product.objects.filter(price__range=(price1, price2))

    return render(request,
                  'shop/product/list.html',
                  {'categories': categories, 'my_products_price': my_products_price, 'cart': cart})


def filter_product_search(request):
    search = request.GET.get('search')
    cart = Cart(request)
    categories = Category.objects.all()
    if not search :
        my_products_search= Product.objects.all()

    else:
       my_products_search = Product.objects.filter(name__icontains=search)


    return render(request,
                  'shop/product/list.html',
                  {'categories': categories, 'my_products_search': my_products_search, 'cart': cart})


def filter_product_color(request):
    color_blue = request.POST.get('color')
    cart = Cart(request)
    categories = Category.objects.all()
    if color_blue=='1' :
        my_products_search = Product.objects.filter(Q(color__color__icontains='green'))

    elif color_blue=='2':
       my_products_search = Product.objects.filter(Q(color__color__icontains='blue'))

    elif color_blue == '3':
        my_products_search = Product.objects.filter(Q(color__color__icontains='yallew'))

    elif color_blue == '4':
        my_products_search = Product.objects.filter(Q(color__color__icontains='red'))

    elif color_blue == '5':
        my_products_search = Product.objects.filter(Q(color__color__icontains='brown'))

    elif color_blue == '6':
        my_products_search = Product.objects.filter(Q(color__color__icontains='black'))

    elif color_blue == '7':
        my_products_search = Product.objects.filter(Q(color__color__icontains='silver'))
    else:
        my_products_search= Product.objects.all()

    return render(request,
                  'shop/product/list.html',
                  {'categories': categories, 'my_products_search': my_products_search, 'cart': cart})


def connect(request):


    return render(request,'shop/connect.html')

