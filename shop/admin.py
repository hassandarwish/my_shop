from django.contrib import admin

# Register your models here.
from django.contrib import admin
from .models import Category, Product, Colors, Size


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(Category, CategoryAdmin)


class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', 'price', 'stock', 'available', 'created', 'updated']
    list_filter = ['available', 'created', 'updated']
    list_editable = ['price', 'stock', 'available']
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(Product, ProductAdmin)


class ColorAdmin(admin.ModelAdmin):
    list_display = ['color']


admin.site.register(Colors, ColorAdmin)


class SizeAdmin(admin.ModelAdmin):
    list_display = ['size']


admin.site.register(Size, SizeAdmin)