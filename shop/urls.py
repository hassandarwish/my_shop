from django.conf.urls import url
from . import views
from django.urls import path

urlpatterns = [
    url(r'^Product_by_price/$', views.filter_product_price, name='filter_product_price'),
    url(r'^Product_by_search/$', views.filter_product_search, name='filter_product_search'),
    url(r'^Product_by_color/$', views.filter_product_color, name='filter_product_color'),
    url(r'^connect/$', views.connect, name='connect'),
    url(r'^Product_by_price/$', views.filter_product_price, name='filter_product_price'),
    url(r'^$', views.product_list, name='product_list'),
    path('<category_slug>/', views.product_list,name='product_list_by_category'),
    url(r'^(?P<id>\d+)/(?P<slug>[-\w]+)/$', views.product_detail, name='product_detail'),


    ]