# Generated by Django 2.0.3 on 2018-05-14 10:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0003_auto_20180514_1041'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='colors',
            name='product',
        ),
        migrations.AddField(
            model_name='product',
            name='product',
            field=models.ManyToManyField(to='shop.Colors'),
        ),
        migrations.AlterField(
            model_name='colors',
            name='color',
            field=models.CharField(db_index=True, max_length=50),
        ),
    ]
