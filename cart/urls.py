from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^all/$', views.cart_detail, name='cart_detail'),

    url(r'^add/(?P<product_id>\d+)/$',views.cart_add,name='cart_add'),
    url(r'^one/(?P<product_id>\d+)/$', views.add_one, name='cart_one'),
    url(r'^remove/(?P<product_id>\d+)/$',views.cart_remove, name='cart_remove'),

    url(r'^add_wish/(?P<product_id>\d+)/$', views.AddWish, name='addwish'),
    url(r'^delete/(?P<product_id>\d+)/$', views.RemoveItem_wishlist, name='deleteItem'),
    url(r'^wishlist/$', views.wishlist, name='wishlist'),
    ]