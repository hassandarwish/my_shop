from .cart import Cart
from django.views.decorators import csrf
from django.views.decorators.csrf import csrf_protect


def cart(request):
    return {'cart': Cart(request)}