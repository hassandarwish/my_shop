from django.http import HttpResponse

from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_POST


from shop.models import Product
from .cart import Cart
from .forms import CartAddProductForm
from cart.forms import CartAddProductForm
from coupons.forms import CouponApplyForm



ls=list()

@require_POST
def cart_add(request, product_id):
    cart = Cart(request)
    product = get_object_or_404(Product, id=product_id)
    form = CartAddProductForm(request.POST)

    if form.is_valid():
        cd = form.cleaned_data
        cart.add(product=product,quantity=cd['quantity'], update_quantity=cd['update'])
    else:
        return HttpResponse('<h1>Page not found</h1>')
    return redirect('cart_detail')

@require_POST
def add_one(request,product_id):
    cart = Cart(request)
    product = get_object_or_404(Product, id=product_id)
    form = CartAddProductForm(request.POST)

    if form.is_valid():
        cd = form.cleaned_data
        cart.add(product=product,quantity=1, update_quantity=cd['update'])
    else:
        return HttpResponse('<h1>Page not found</h1>')
    return redirect('product_list')

def cart_remove(request, product_id):
    cart = Cart(request)
    product = get_object_or_404(Product, id=product_id)
    cart.remove(product)
    return redirect('cart_detail')


def cart_detail(request):
    cart = Cart(request)
    for item in cart:
        item['update_quantity_form'] = CartAddProductForm(
            initial={'quantity': item['quantity'],
                     'update': True})
    coupon_apply_form = CouponApplyForm()
    return render(request, 'cart/detail.html',
                  {'cart': cart, 'coupon_apply_form': coupon_apply_form
                        })


def product_detail(request, id, slug):
    product = get_object_or_404(Product, id=id,slug=slug,available=True)
    cart_product_form = CartAddProductForm()
    return render(request,'shop/product/detail.html',
                  {'product': product,
                   'cart_product_form': cart_product_form,
                   })



##############################################


ls=list()
def AddWish(request,product_id):
    cart = Cart(request)
    if product_id not in ls:
        ls.append(product_id)
        products = Product.objects.filter(id__in=ls)
        ss=len(ls)
        return redirect('product_list')
    else:
        products = Product.objects.filter(id__in=ls)
        ss = len(ls)
        return redirect('product_list')

def RemoveItem_wishlist(request,product_id):

    ls.remove(product_id)

    return redirect('wishlist')

def wishlist(request):
    cart = Cart(request)

    products = Product.objects.filter(id__in=ls)

    return render(request, 'cart/wishlist.html',
                  {'products': products, 'cart': cart})